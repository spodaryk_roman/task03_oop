package ua.lviv.iot.manager;

import ua.lviv.iot.enums.Place;
import ua.lviv.iot.enums.TypeOfHoliday;
import ua.lviv.iot.models.Holiday;

import java.util.ArrayList;
import java.util.List;

public class HolidayManager {

    public List<Holiday> findHolidaysByPlace(Place place, List<Holiday> lostOfAllHolidays) {
        List<Holiday> listOfHolidays = new ArrayList<Holiday>();
        for (Holiday list : lostOfAllHolidays) {
            if (list.getPlaceOfHoliday().equals(place)) {
                listOfHolidays.add(list);
            }
        }


        return sortHolidaysByIncreasingPrice(listOfHolidays);
    }

    public List<Holiday> findHolidaysByType(TypeOfHoliday type, List<Holiday> lostOfAllHolidays) {
        List<Holiday> listOfHolidays = new ArrayList<>();
        for (Holiday list : lostOfAllHolidays) {
            if (list.getTypeOfHoliday().equals(type)) {
                System.out.println("!!!!!!! list.getTypeOfHoliday() = " + list.getTypeOfHoliday() + " type = " + type);
                listOfHolidays.add(list);
            }
        }
        return sortHolidaysByIncreasingPrice(listOfHolidays);
    }

    public Holiday findHolidaysWithTheHighestPrice(List<Holiday> lostOfAllHolidays) {
        double maxPrice = 0.0;
        Holiday holiday = null;
        for (Holiday list : lostOfAllHolidays) {
            if (list.getPriceForHoliday() > maxPrice) {
                holiday = list;
                maxPrice = list.getPriceForHoliday();
            }
        }
        return holiday;
    }

    public Holiday findHolidaysWithTheLowestPrice(List<Holiday> lostOfAllHolidays) {
        if (lostOfAllHolidays.size() != 0) {
            double maxPrice = lostOfAllHolidays.get(lostOfAllHolidays.size() - 1).getPriceForHoliday();
            Holiday holiday = null;
            for (Holiday list : lostOfAllHolidays) {
                if (list.getPriceForHoliday() < maxPrice) {
                    holiday = list;
                    maxPrice = list.getPriceForHoliday();
                }
            }
            return holiday;
        } else {
            System.out.println("there is any holiday in list");
            return null;
        }

    }

    public List<Holiday> sortHolidaysByIncreasingPrice(List<Holiday> lostOfAllHolidays) {
        lostOfAllHolidays.sort((o1, o2) -> (int) o1.getPriceForHoliday() - (int) o2.getPriceForHoliday());
        return lostOfAllHolidays;
    }

    public List<Holiday> sortHolidaysByDecreasingPrice(List<Holiday> lostOfAllHolidays) {
        lostOfAllHolidays.sort((o1, o2) -> (int) o2.getPriceForHoliday() - (int) o1.getPriceForHoliday());
        return lostOfAllHolidays;
    }
}
