package ua.lviv.iot.view;

import ua.lviv.iot.controller.ControllerImpl;
import ua.lviv.iot.enums.Place;
import ua.lviv.iot.enums.TypeOfHoliday;
import ua.lviv.iot.models.Holiday;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private ControllerImpl controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private List<Holiday> listOfHoliday;


    public MyView(List<Holiday> list) {
        this.listOfHoliday = list;
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - findHolidays with BIRTHDAY type");
        menu.put("2", "  2 - findHolidays with NEW_YEAR type");
        menu.put("3", "  3 - findHolidays with BEECH_PARTY type");
        menu.put("4", "  4 - findHolidays with PARK place");
        menu.put("5", "  5 - findHolidays with STREET place");
        menu.put("6", "  6 - findHolidays with HOME place");
        menu.put("7", "  7 - findHolidays with CHILDREN_GARDEN place");
        menu.put("9", "  8 - sortHolidaysByIncreasingPrice and print");
        menu.put("10", "  9 - sortHolidaysByDecreasingPrice and print");
        menu.put("11", "  10 - findHolidaysWithTheHighestPrice and print");
        menu.put("12", "  11 - findHolidaysWithTheLowestPrice and print");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
        methodsMenu.put("9", this::pressButton9);
        methodsMenu.put("10", this::pressButton10);
        methodsMenu.put("11", this::pressButton11);
    }

    private void pressButton1() {
        List<Holiday> list = controller.findHolidaysByType(TypeOfHoliday.BIRTHDAY, listOfHoliday);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    private void pressButton2() {
        List<Holiday> list = controller.findHolidaysByType(TypeOfHoliday.NEW_YEAR, listOfHoliday);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    private void pressButton3() {
        List<Holiday> list = controller.findHolidaysByType(TypeOfHoliday.BEECH_PARTY, listOfHoliday);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    private void pressButton4() {
        List<Holiday> list = controller.findHolidaysByPlace(Place.PARK, listOfHoliday);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    private void pressButton5() {
        List<Holiday> list = controller.findHolidaysByPlace(Place.STREET, listOfHoliday);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    private void pressButton6() {
        List<Holiday> list = controller.findHolidaysByPlace(Place.HOME, listOfHoliday);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    private void pressButton7() {
        List<Holiday> list = controller.findHolidaysByPlace(Place.CHILDREN_GARDEN, listOfHoliday);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

    private void pressButton8() {
        controller.sortHolidaysByIncreasingPrice(listOfHoliday);
        for (Holiday holiday : listOfHoliday) {
            System.out.println(holiday);
        }
    }

    private void pressButton9() {
        controller.sortHolidaysByDecreasingPrice(listOfHoliday);
        for (Holiday holiday : listOfHoliday) {
            System.out.println(holiday);
        }
    }

    private void pressButton10() {
        Holiday holiday = controller.findHolidaysWithTheHighestPrice(listOfHoliday);
        System.out.println(holiday);
    }

    private void pressButton11() {
        Holiday holiday = controller.findHolidaysWithTheLowestPrice(listOfHoliday);
        System.out.println(holiday);
    }


    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.\n\n");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
