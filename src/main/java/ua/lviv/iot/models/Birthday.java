package ua.lviv.iot.models;

import ua.lviv.iot.enums.Place;
import ua.lviv.iot.enums.TypeOfHoliday;

public class Birthday extends Holiday {
    private int ageOfBirthdayMan;
    private boolean presenceOfCake;

    public Birthday() {
    }

    public Birthday(String nameOfHoliday, double priceForHoliday, Place placeOfHoliday, TypeOfHoliday typeOfHoliday,
                    double durationOfHoliday) {
        super(nameOfHoliday, priceForHoliday, placeOfHoliday, typeOfHoliday, durationOfHoliday);
    }

    public Birthday(String nameOfHoliday, double priceForHoliday, Place placeOfHoliday, TypeOfHoliday typeOfHoliday,
                    double durationOfHoliday, int ageOfBirthdayMan, boolean presenceOfCake) {
        super(nameOfHoliday, priceForHoliday, placeOfHoliday, typeOfHoliday, durationOfHoliday);
        this.ageOfBirthdayMan = ageOfBirthdayMan;
        this.presenceOfCake = presenceOfCake;
    }

    public int getAgeOfBirthdayMan() {
        return ageOfBirthdayMan;
    }

    public void setAgeOfBirthdayMan(int ageOfBirthdayMan) {
        this.ageOfBirthdayMan = ageOfBirthdayMan;
    }

    public boolean isPresenceOfCake() {
        return presenceOfCake;
    }

    public void setPresenceOfCake(boolean presenceOfCake) {
        this.presenceOfCake = presenceOfCake;
    }

    @Override
    public String toString() {
        return "Birthday{" + super.toString() +
                "ageOfBirthdayMan=" + ageOfBirthdayMan +
                ", presenceOfCake=" + presenceOfCake +
                '}';
    }
}
