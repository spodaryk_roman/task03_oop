package ua.lviv.iot.models;

import ua.lviv.iot.enums.Place;
import ua.lviv.iot.enums.TypeOfHoliday;

public class LastBell extends Holiday {
    private boolean presenceOfFlowersForTeacher;
    private int numberOfTeachers;

    public LastBell() {
    }

    public LastBell(String nameOfHoliday, double priceForHoliday, Place placeOfHoliday, TypeOfHoliday typeOfHoliday,
                    double durationOfHoliday) {
        super(nameOfHoliday, priceForHoliday, placeOfHoliday, typeOfHoliday, durationOfHoliday);
    }

    public LastBell(String nameOfHoliday, double priceForHoliday, Place placeOfHoliday, TypeOfHoliday typeOfHoliday,
                    double durationOfHoliday, boolean presenceOfFlowersForTeacher, int numberOfTeachers) {
        super(nameOfHoliday, priceForHoliday, placeOfHoliday, typeOfHoliday, durationOfHoliday);
        this.presenceOfFlowersForTeacher = presenceOfFlowersForTeacher;
        this.numberOfTeachers = numberOfTeachers;
    }

    public boolean isPresenceOfFlowersForTeacher() {
        return presenceOfFlowersForTeacher;
    }

    public void setPresenceOfFlowersForTeacher(boolean presenceOfFlowersForTeacher) {
        this.presenceOfFlowersForTeacher = presenceOfFlowersForTeacher;
    }

    public int getNumberOfTeachers() {
        return numberOfTeachers;
    }

    public void setNumberOfTeachers(int numberOfTeachers) {
        this.numberOfTeachers = numberOfTeachers;
    }

    @Override
    public String toString() {
        return "LastBell{" + super.toString() +
                "presenceOfFlowersForTeacher=" + presenceOfFlowersForTeacher +
                ", numberOfTeachers=" + numberOfTeachers +
                '}';
    }
}
