package ua.lviv.iot.models;

import ua.lviv.iot.enums.Place;
import ua.lviv.iot.enums.TypeOfHoliday;

public class Holiday {
    private String nameOfHoliday;
    private double priceForHoliday;
    private Place placeOfHoliday;
    private TypeOfHoliday typeOfHoliday;
    private double durationOfHoliday;

    public Holiday() {
    }

    public Holiday(String nameOfHoliday, double priceForHoliday, Place placeOfHoliday, TypeOfHoliday typeOfHoliday, double durationOfHoliday) {
        this.nameOfHoliday = nameOfHoliday;
        this.priceForHoliday = priceForHoliday;
        this.placeOfHoliday = placeOfHoliday;
        this.typeOfHoliday = typeOfHoliday;
        this.durationOfHoliday = durationOfHoliday;
    }

    public String getNameOfHoliday() {
        return nameOfHoliday;
    }

    public void setNameOfHoliday(String nameOfHoliday) {
        this.nameOfHoliday = nameOfHoliday;
    }

    public double getPriceForHoliday() {
        return priceForHoliday;
    }

    public void setPriceForHoliday(double priceForHoliday) {
        this.priceForHoliday = priceForHoliday;
    }

    public Place getPlaceOfHoliday() {
        return placeOfHoliday;
    }

    public void setPlaceOfHoliday(Place placeOfHoliday) {
        this.placeOfHoliday = placeOfHoliday;
    }

    public TypeOfHoliday getTypeOfHoliday() {
        return typeOfHoliday;
    }

    public void setTypeOfHoliday(TypeOfHoliday typeOfHoliday) {
        this.typeOfHoliday = typeOfHoliday;
    }

    @Override
    public String toString() {
        return "Holiday{" +
                "nameOfHoliday='" + nameOfHoliday + '\'' +
                ", priceForHoliday=" + priceForHoliday +
                ", placeOfHoliday=" + placeOfHoliday +
                ", typeOfHoliday=" + typeOfHoliday +
                ", durationOfHoliday=" + durationOfHoliday +
                '}';
    }
}
