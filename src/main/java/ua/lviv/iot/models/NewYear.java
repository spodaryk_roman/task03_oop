package ua.lviv.iot.models;

import ua.lviv.iot.enums.Place;
import ua.lviv.iot.enums.TypeOfHoliday;

public class NewYear extends Holiday {
    private boolean presenceOfChristmasTree;
    private boolean presenceOfSanta;
    private int numberOfGifts;

    public NewYear() {
    }

    public NewYear(String nameOfHoliday, double priceForHoliday, Place placeOfHoliday, TypeOfHoliday typeOfHoliday,
                   double durationOfHoliday) {
        super(nameOfHoliday, priceForHoliday, placeOfHoliday, typeOfHoliday, durationOfHoliday);
    }

    public NewYear(String nameOfHoliday, double priceForHoliday, Place placeOfHoliday, TypeOfHoliday typeOfHoliday,
                   double durationOfHoliday, boolean presenceOfChristmasTree, boolean presenceOfSanta, int numberOfGifts) {
        super(nameOfHoliday, priceForHoliday, placeOfHoliday, typeOfHoliday, durationOfHoliday);
        this.presenceOfChristmasTree = presenceOfChristmasTree;
        this.presenceOfSanta = presenceOfSanta;
        this.numberOfGifts = numberOfGifts;
    }

    public boolean isPresenceOfChristmasTree() {
        return presenceOfChristmasTree;
    }

    public void setPresenceOfChristmasTree(boolean presenceOfChristmasTree) {
        this.presenceOfChristmasTree = presenceOfChristmasTree;
    }

    public boolean isPresenceOfSanta() {
        return presenceOfSanta;
    }

    public void setPresenceOfSanta(boolean presenceOfSanta) {
        this.presenceOfSanta = presenceOfSanta;
    }

    public int getNumberOfGifts() {
        return numberOfGifts;
    }

    public void setNumberOfGifts(int numberOfGifts) {
        this.numberOfGifts = numberOfGifts;
    }

    @Override
    public String toString() {
        return "NewYear{" + super.toString() +
                "presenceOfChristmasTree=" + presenceOfChristmasTree +
                ", presenceOfSanta=" + presenceOfSanta +
                ", numberOfGifts=" + numberOfGifts +
                '}';
    }
}
