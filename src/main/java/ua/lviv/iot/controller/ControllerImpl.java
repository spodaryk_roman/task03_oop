package ua.lviv.iot.controller;

import ua.lviv.iot.enums.Place;
import ua.lviv.iot.enums.TypeOfHoliday;
import ua.lviv.iot.manager.HolidayManager;
import ua.lviv.iot.models.Holiday;

import java.util.List;

public class ControllerImpl implements Controller {
    private HolidayManager manager;

    public ControllerImpl() {
        this.manager = new HolidayManager();
    }

    @Override
    public List<Holiday> findHolidaysByType(TypeOfHoliday type, List<Holiday> lostOfAllHolidays) {
        return manager.findHolidaysByType(type, lostOfAllHolidays);
    }

    @Override
    public List<Holiday> findHolidaysByPlace(Place place, List<Holiday> lostOfAllHolidays) {
        return manager.findHolidaysByPlace(place, lostOfAllHolidays);
    }

    @Override
    public List<Holiday> sortHolidaysByIncreasingPrice(List<Holiday> lostOfAllHolidays) {
        return manager.sortHolidaysByIncreasingPrice(lostOfAllHolidays);
    }

    @Override
    public List<Holiday> sortHolidaysByDecreasingPrice(List<Holiday> lostOfAllHolidays) {
        return manager.sortHolidaysByDecreasingPrice(lostOfAllHolidays);
    }

    @Override
    public Holiday findHolidaysWithTheHighestPrice(List<Holiday> lostOfAllHolidays) {
        return manager.findHolidaysWithTheHighestPrice(lostOfAllHolidays);
    }

    @Override
    public Holiday findHolidaysWithTheLowestPrice(List<Holiday> lostOfAllHolidays) {
        return manager.findHolidaysWithTheLowestPrice(lostOfAllHolidays);
    }
}
