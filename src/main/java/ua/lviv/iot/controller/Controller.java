package ua.lviv.iot.controller;

import ua.lviv.iot.enums.Place;
import ua.lviv.iot.enums.TypeOfHoliday;
import ua.lviv.iot.models.Holiday;

import java.util.List;

public interface Controller {
    List<Holiday> findHolidaysByType(TypeOfHoliday type, List<Holiday> lostOfAllHolidays);

    List<Holiday> findHolidaysByPlace(Place place, List<Holiday> lostOfAllHolidays);

    List<Holiday> sortHolidaysByIncreasingPrice(List<Holiday> lostOfAllHolidays);

    List<Holiday> sortHolidaysByDecreasingPrice(List<Holiday> lostOfAllHolidays);

    Holiday findHolidaysWithTheHighestPrice(List<Holiday> lostOfAllHolidays);

    Holiday findHolidaysWithTheLowestPrice(List<Holiday> lostOfAllHolidays);
}