package ua.lviv.iot.enums;

public enum TypeOfHoliday {
    BIRTHDAY,
    NEW_YEAR,
    BEECH_PARTY
}
