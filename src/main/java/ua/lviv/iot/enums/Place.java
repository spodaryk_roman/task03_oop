package ua.lviv.iot.enums;

public enum Place {
    STREET,
    HOME,
    PARK,
    CHILDREN_GARDEN
}
