package ua.lviv.iot;

import ua.lviv.iot.enums.Place;
import ua.lviv.iot.enums.TypeOfHoliday;
import ua.lviv.iot.models.Birthday;
import ua.lviv.iot.models.Holiday;
import ua.lviv.iot.models.LastBell;
import ua.lviv.iot.models.NewYear;
import ua.lviv.iot.view.MyView;

import java.util.ArrayList;
import java.util.List;

public class Application {

    private static List<Holiday> list = new ArrayList<>();

    private static void init() {
        list.add(new Holiday("holidddaaaayy", 340.5, Place.CHILDREN_GARDEN, TypeOfHoliday.NEW_YEAR, 3.2));
        list.add(new LastBell("Останній Дзвоник", 200.5, Place.PARK, TypeOfHoliday.BEECH_PARTY, 1.0));
        list.add(new LastBell("Наш Дзвоник", 100.0, Place.STREET, TypeOfHoliday.BEECH_PARTY, 3.0));
        list.add(new LastBell("Дзвоник", 690.8, Place.PARK, TypeOfHoliday.BEECH_PARTY, 2.0));
        list.add(new Birthday("My Birthday", 6100.8, Place.HOME, TypeOfHoliday.BIRTHDAY, 10.0));
        list.add(new NewYear("2020 New Year!", 10100.8, Place.HOME, TypeOfHoliday.NEW_YEAR, 20.0));
        list.add(new Birthday("Birthday", 3100.8, Place.CHILDREN_GARDEN, TypeOfHoliday.BIRTHDAY, 70.0));
    }

    public static void main(String[] args) {
        init();
        new MyView(list).show();
    }
}
